#!/bin/bash

# If ssh service is installed
if [ -f "/etc/init.d/ssh" ]; then
	# Start ssh
	service ssh start
fi

# Exec CMD
exec "$@"