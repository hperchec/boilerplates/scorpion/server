<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // test user
        User::create([
            'id' => 1,
            'password' => bcrypt('test'),
            'email' => 'example@test.com',
            'firstname' => 'John',
            'lastname' => 'Doe',
            'email_verified_at' => null
        ]);

    }
}
