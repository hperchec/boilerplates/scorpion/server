<?php

namespace App\Rules;

use Illuminate\Support\Str;
use Illuminate\Contracts\Validation\Rule;

class PasswordPolicy implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // At least 10 characters
        $lengthPasses = (Str::length($value) >= 10);
        // At least 1 lowercase letter
        $lowercasePasses = (Str::upper($value) !== $value);
        // At least 1 uppercase letter
        $uppercasePasses = (Str::lower($value) !== $value);
        // At least 1 numerical digit
        $numericPasses = ((bool) preg_match('/[0-9]/', $value));
        // At least 1 special character
        $specialCharacterPasses = ((bool) preg_match('/[^A-Za-z0-9]/', $value));

        return ($lengthPasses && $lowercasePasses && $uppercasePasses && $numericPasses && $specialCharacterPasses);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return [ 
            [ 'rule:PasswordPolicy' => 'Password must be at least 10 characters in length and include at least: 1 upper-case letter, 1 lower-case letter, 1 numerical digit and 1 special character.' ]
        ];

    }
}
