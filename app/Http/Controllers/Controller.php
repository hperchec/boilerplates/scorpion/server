<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @OA\Info (
     *     version="1.0.0",
     *     title="App API Swagger",
     *     description="App project API documentation",
     * )
     *
     * @OA\Server(
     *     url=API_HOST,
     *     description="Local API Server"
     * )
     * 
     * @OA\Tag(
     *     name="Auth",
     *     description="Specific routes for authentication"
     * ),
     * @OA\Tag(
     *     name="Users",
     *     description="Specific routes for users"
     * )
     *
    */
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
