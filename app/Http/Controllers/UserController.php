<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Auth\Events\Registered;
use \Image;

use App\Models\Error;
use App\Models\User;

use App\Http\Requests\User\CreateRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Http\Requests\User\UpdateThumbnailRequest;
use App\Http\Requests\User\GetUserThumbnailRequest;
use App\Http\Requests\User\CheckEmailRequest;
use App\Http\Requests\User\ResendEmailVerificationLinkRequest;

class UserController extends Controller
{
    /**
     * Create a new controller instance
     * @codeCoverageIgnore
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @OA\Post(
     *     path="/users/create",
     *     operationId="users.create",
     *     summary="Create user",
     *     description="**Créer un nouvel utilisateur**",
     *     tags={"Users"},
     *     @OA\RequestBody(
     *         description="User data to create",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Requests.User.Create")
     *         )
     *     ),
     *     @OA\Response(response=200, description="Successful operation", @OA\MediaType(mediaType="application/json")),
     *     @OA\Response(response=422, description="Unprocessable entity")
     *  )
     * 
     * ----
     *
     * Create new user
     * @param CreateRequest $request - The request
     * @return User|Response
     */
    public function create (CreateRequest $request)
    {
        // Validate the request
        $validated = $request->validated();
        // New user
        $newUser = new User();
        // Firstname
        $newUser->firstname = $validated['firstname'];
        // Lastname
        $newUser->lastname = $validated['lastname'];
        // Email
        $newUser->email = $validated['email'];
        // Password
        $newUser->password = Hash::make($validated['password']);
        // Save
        $newUser->save();
        // Emit 'registered' event
        event(new Registered($newUser));
        // Return created user
        return $newUser;
    }

    /**
     * @OA\Patch(
     *     path="/users/{userId}",
     *     operationId="users.update",
     *     summary="Update user",
     *     description="**Modifier l'utilisateur courant**  
               ❗ Action limitée au compte utilisateur authentifié par token
           ",
     *     tags={"Users"},
     *     @OA\Parameter(
     *         name="userId",
     *         in="path",
     *         description="Identifiant de l'utilisateur (ID)",
     *         required=true
     *     ),
     *     @OA\RequestBody(
     *         description="User data to update",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Requests.User.Update")
     *         )
     *     ),
     *     @OA\Response(response=200, description="Successful operation"),
     *     @OA\Response(response=401, description="Unauthenticated"),
     *     @OA\Response(response=403, description="Forbidden"),
     *     @OA\Response(response=404, description="Not found"),
     *     @OA\Response(response=422, description="Unprocessable entity")
     *  )
     * 
     * ----
     *
     * Update existing user
     * @param UpdateRequest $request - The request
     * @param User $user - The request
     * @return User|Response
     */
    public function update (UpdateRequest $request, User $user)
    {
        // Validate the request
        $validated = $request->validated();
        // Password security
        $doesPasswordMatches = Hash::check($validated['password'], $user->password);
        if (!$doesPasswordMatches) {
            return errorResponse(401, Error::find('e0022'));
        }
        // Assign each new property value
        foreach ($validated as $key => $value) {
            // Ignore password
            if ($key !== 'password') {
                $user->$key = $value;
            }
        }
        // Save and return modified user
        $user->save();
        return $user;
    }

    /**
     * @OA\Post(
     *     path="/users/{userId}/thumbnail",
     *     operationId="users.update.thumbnail",
     *     summary="Update authenticated user thumbnail",
     *     description="**Modifier la miniature (image de profil) de l'utilisateur courant**  
               ❗ Action limitée au compte utilisateur authentifié par token
           ",
     *     tags={"Users"},
     *     @OA\Parameter(
     *         name="userId",
     *         in="path",
     *         description="Identifiant de l'utilisateur (ID)",
     *         required=true
     *     ),
     *     @OA\RequestBody(
     *         description="User thumbnail data to update",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(ref="#/components/schemas/Requests.User.UpdateThumbnail")
     *         )
     *     ),
     *     @OA\Response(response=200, description="Successful operation"),
     *     @OA\Response(response=401, description="Unauthenticated"),
     *     @OA\Response(response=403, description="Forbidden"),
     *     @OA\Response(response=404, description="Not found"),
     *     @OA\Response(response=422, description="Unprocessable entity")
     *  )
     * 
     * ----
     * Update thumbnail of an existing user
     * @param UpdateThumbnailRequest $request - The request
     * @param User $user - The request
     * @return Response
     */
    public function updateThumbnail (UpdateThumbnailRequest $request, User $user)
    {
        // Validate the request
        $validated = $request->validated();
        // Password security
        $doesPasswordMatches = Hash::check($validated['password'], $user->password);
        if (!$doesPasswordMatches) {
            return errorResponse(401, Error::find('e0022'));
        }
        // Prepare thumbnail
        $tmp_thumbnail = $validated['thumbnail'];
        // Get extension
        $imageType = $tmp_thumbnail->getClientOriginalExtension();
        // Build file name
        $fileName = $user->generateThumbnailFileName($imageType);
        // Resize to 250x250
        ini_set('memory_limit', '256M'); // IMPORTANT !! because resize()
        $tmp_thumbnail = Image::make($tmp_thumbnail)->resize(250, 250);
        // Save file
        $tmp_thumbnail->save(storage_path('app/thumbnails/' . $fileName), 100); // 100: Full quality
        $tmp_thumbnail->destroy();
        // Delete old thumbnail
        Storage::disk('thumbnails')->delete($user->thumbnail);
        // Save modified user
        $user->thumbnail = $fileName;
        $user->save();
        return response('Success', 200);
    }

    /**
     * @OA\Get(
     *     path="/users/{userId}/thumbnail",
     *     operationId="user.thumbnail",
     *     summary="Retrieve an user thumbnail",
     *     description="**Retrouver la miniature de profil d'un utilisateur**",
     *     tags={"Users"},
     *     @OA\Parameter(
     *         name="userId",
     *         in="path",
     *         description="Identifiant de l'utilisateur (ID)",
     *         required=true
     *     ),
     *     @OA\Response(response=200, description="Successful operation", @OA\MediaType(mediaType="image/*")),
     *     @OA\Response(response=401, description="Unauthenticated"),
     *     @OA\Response(response=403, description="Forbidden"),
     *     @OA\Response(response=404, description="Not found")
     * )
     *
     * ----
     * 
     * Return the thumbnail image (in MIME Type format)
     * @param GetUserThumbnailRequest $request - The request
     * @param User $user - The user id
     * @return Response
     */
    public function getUserThumbnail (GetUserThumbnailRequest $request, User $user)
    {
        // Get file path
        $thumbnailFile = $user->thumbnail
            ? storage_path('app/thumbnails/' . $user->thumbnail)
            : storage_path('app/thumbnails/anonymous.png');
        // Return image as MIME Type
        return Image::make($thumbnailFile)->response();
    }

    /**
     * @OA\Post(
     *     path="/users/check-email",
     *     operationId="users.checkEmail",
     *     summary="Check email availability",
     *     description="**Tester la disponibilité d'une adresse mail**",
     *     tags={"Users"},
     *     @OA\RequestBody(
     *         description="Email data to check",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Requests.User.CheckEmail")
     *         )
     *     ),
     *     @OA\Response(response=200, description="Successful operation", @OA\MediaType(mediaType="application/json")),
     *     @OA\Response(response=409, description="Conflict"),
     *     @OA\Response(response=422, description="Unprocessable entity")
     *  )
     * 
     * ----
     * 
     * Chech if email is already taken
     * @param CheckEmailRequest $request - The request
     * @return Response
     */
    public function checkEmail (CheckEmailRequest $request)
    {
        // Validate the request
        $validated = $request->validated();
        // Retrieve user
        $found = User::where('email', $validated['email'])->first();
        return $found
            ? errorResponse(409, Error::find('e0031'))
            : response('Success', 200);
    }

    /**
     * @OA\Post(
     *     path="/users/{userId}/email/verify/{hash}",
     *     operationId="verification.verify",
     *     summary="Verify user mail address",
     *     description="**Vérification de l'adresse email de l'utilisateur**
               ❗ Action limitée au compte utilisateur authentifié par token
           ",
     *     tags={"Users"},
     *     @OA\Parameter(
     *         name="userId",
     *         in="path",
     *         description="Identifiant de l'utilisateur (ID)",
     *         required=true
     *     ),
     *     @OA\Parameter(
     *         name="hash",
     *         in="path",
     *         description="Hash (lien temporaire)",
     *         required=true
     *     ),
     *     @OA\Response(response=200, description="Successful operation"),
     *     @OA\Response(response=401, description="Unauthenticated"),
     *     @OA\Response(response=403, description="Forbidden"),
     *     @OA\Response(response=404, description="Not found")
     *  )
     * 
     * ----
     * 
     * Verify user email
     * @param EmailVerificationRequest $request - The request
     * @return Response
     */
    public function verifyEmail (EmailVerificationRequest $request) {
        // Verify email
        $request->fulfill();
        return response('Success', 200);
    }

    /**
     * @OA\Post(
     *     path="/users/{userId}/email/resend-verification-link",
     *     operationId="verification.send",
     *     summary="Resend mail verification link",
     *     description="**Renvoyer le lien de vérification de l'adresse email de l'utilisateur**
               ❗ Action limitée au compte utilisateur authentifié par token
           ",
     *     tags={"Users"},
     *     @OA\Parameter(
     *         name="userId",
     *         in="path",
     *         description="Identifiant de l'utilisateur (ID)",
     *         required=true
     *     ),
     *     @OA\Response(response=200, description="Successful operation"),
     *     @OA\Response(response=401, description="Unauthenticated"),
     *     @OA\Response(response=403, description="Forbidden"),
     *     @OA\Response(response=404, description="Not found")
     *  )
     * 
     * ----
     * 
     * Verify user email
     * @param ResendEmailVerificationLinkRequest $request - The request
     * @param User $user - The user id
     * @return Response
     */
    public function resendEmailVerificationLink (ResendEmailVerificationLinkRequest $request, User $user) {
        $user->sendEmailVerificationNotification();
        return response('Verification link sent!');
    }

}
