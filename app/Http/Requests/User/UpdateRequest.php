<?php

namespace App\Http\Requests\User;

use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

use App\Http\Requests\BaseRequest;

/**
 * @OA\Schema(schema="Requests.User.Update") {
 *     required={
 *         "password"
 *     }
 * }
 */
class UpdateRequest extends BaseRequest
{

    /**
     * Determine if the user is authorized to make this request
     * @return bool
     */
    public function authorize()
    {
        // Authorize -> App\Policies\UserPolicy::update
        Gate::authorize('update', $this->route('user'));
        return true;
    }

    /**
     * @OA\Property(
     *     property="firstname",
     *     type="string",
     *     description="Prénom de l'utilisateur"
     * ),
     * @OA\Property(
     *     property="lastname",
     *     type="string",
     *     description="Nom de l'utilisateur"
     * ),
     * @OA\Property(
     *     property="password",
     *     type="string",
     *     description="Mot de passe de l'utilisateur"
     * )
     *
     * ----
     * 
     * Get the validation rules that apply to the request
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'string|max:255',
            'lastname' => 'string|max:255',
            'password' => 'required|string|max:255' // Required for security
        ];
    }

    /**
     * Get the error messages for the defined validation rules
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
