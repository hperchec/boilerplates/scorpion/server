<?php

namespace App\Http\Requests\User;

use Illuminate\Validation\Rule;

use App\Http\Requests\BaseRequest;
use App\Models\User;
use App\Rules\PasswordPolicy;

/**
 * @OA\Schema(schema="Requests.User.Create") {
 *     required={
 *         "firstname",
 *         "lastname",
 *         "email",
 *         "password",
 *         "password_confirmation"
 *     }
 * }
 */
class CreateRequest extends BaseRequest
{

    /**
     * Determine if the user is authorized to make this request
     * @return bool
     */
    public function authorize()
    {
        // PUBLIC ROUTE
        return true;
    }

    /**
     * @OA\Property(
     *     property="firstname",
     *     type="string",
     *     description="Prénom de l'utilisateur"
     * ),
     * @OA\Property(
     *     property="lastname",
     *     type="string",
     *     description="Nom de l'utilisateur"
     * ),
     * @OA\Property(
     *     property="email",
     *     type="string",
     *     description="Adresse mail de l'utilisateur"
     * ),
     * @OA\Property(
     *     property="password",
     *     type="string",
     *     description="Mot de passe de l'utilisateur"
     * ),
     * @OA\Property(
     *     property="password_confirmation",
     *     type="string",
     *     description="Confirmation du mot de passe de l'utilisateur"
     * )
     *
     * ----
     * 
     * Get the validation rules that apply to the request
     * @return array
     */
    public function rules()
    {
        return [
            // Required
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'password' => [
                'required',
                'string',
                'confirmed',
                new PasswordPolicy,
                'max:255'
            ],
            'email' => 'required|string|unique:users|email:filter|max:255'
        ];
    }

    /**
     * Get the error messages for the defined validation rules
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
