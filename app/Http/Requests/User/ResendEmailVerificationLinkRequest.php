<?php

namespace App\Http\Requests\User;

use Illuminate\Validation\Rule;

use App\Http\Requests\BaseRequest;
use App\Models\Error;

/**
 * @OA\Schema(schema="Requests.User.ResendEmailVerificationLink")
 */
class ResendEmailVerificationLinkRequest extends BaseRequest
{

    /**
     * Determine if the user is authorized to make this request
     * @return bool
     */
    public function authorize()
    {
        // Authorize -> App\Policies\UserPolicy::resendEmailVerificationLink
        Gate::authorize('resend-email-verification-link', $this->route('user'));
        return true;
    }

    /**
     * Get the validation rules that apply to the request
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Get the error messages for the defined validation rules
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
