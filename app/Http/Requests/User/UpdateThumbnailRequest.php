<?php

namespace App\Http\Requests\User;

use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

use App\Http\Requests\BaseRequest;

/**
 * @OA\Schema(schema="Requests.User.UpdateThumbnail") {
 *     required={
 *         "password",
 *         "thumbnail"
 *     }
 * }
 */
class UpdateThumbnailRequest extends BaseRequest
{

    /**
     * Determine if the user is authorized to make this request
     * @return bool
     */
    public function authorize()
    {
        // Authorize -> App\Policies\UserPolicy::updateThumbnail
        Gate::authorize('update-thumbnail', $this->route('user'));
        return true;
    }

    /**
     * @OA\Property(
     *     property="password",
     *     type="string",
     *     description="Mot de passe de l'utilisateur"
     * ),
     * @OA\Property(
     *     property="thumbnail",
     *     type="string",
     *     format="binary",
     *     description="Miniature (image de profil) de l'utilisateur  
               ❗ *MIMES types accepted: jpeg, png, jpg, gif, svg*  
               ❗ *MAX size: 2048 bytes*
     *     "
     * )
     * 
     * ----
     * 
     * Get the validation rules that apply to the request
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|string|max:255',
            'thumbnail' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048' // Accepts only jpg and png format
        ];
    }

    /**
     * Get the error messages for the defined validation rules
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
