<?php

namespace App\Http\Requests\Auth;

use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

use App\Http\Requests\BaseRequest;
use App\Rules\PasswordPolicy;

/**
 * @OA\Schema(schema="Requests.Auth.UpdatePassword") {
 *     required={
 *         "current_password",
 *         "password",
 *         "password_confirmation"
 *     }
 * }
 */
class UpdatePasswordRequest extends BaseRequest
{

    /**
     * Determine if the user is authorized to make this request
     * @return bool
     */
    public function authorize()
    {
        // Authorize -> App\Policies\AuthPolicy::updatePassword
        Gate::authorize('update-password');
        return true;
    }

    /**
     * @OA\Property(
     *     property="current_password",
     *     type="string",
     *     description="Ancien mot de passe de l'utilisateur"
     * ),
     * @OA\Property(
     *     property="password",
     *     type="string",
     *     description="Mot de passe de l'utilisateur"
     * ),
     * @OA\Property(
     *     property="password_confirmation",
     *     type="string",
     *     description="Confirmation du mot de passe de l'utilisateur"
     * )
     * 
     * ----
     * 
     * Get the validation rules that apply to the request
     * @return array
     */
    public function rules()
    {
        return [
            'current_password' => 'required|string|max:255',
            'password' => [
                'required',
                'string',
                'confirmed',
                new PasswordPolicy,
                'max:255'
            ]
        ];
    }

    /**
     * Get the error messages for the defined validation rules
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
