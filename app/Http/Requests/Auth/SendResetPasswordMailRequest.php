<?php

namespace App\Http\Requests\Auth;

use Illuminate\Validation\Rule;

use App\Http\Requests\BaseRequest;
use App\Models\Error;

/**
 * @OA\Schema(schema="Requests.Auth.SendResetPasswordMail") {
 *     required={
 *         "email"
 *     }
 * }
 */
class SendResetPasswordMailRequest extends BaseRequest
{

    /**
     * Determine if the user is authorized to make this request
     * @return bool
     */
    public function authorize()
    {
        // PUBLIC ROUTE
        return true;
    }

    /** 
     * @OA\Property(
     *     property="email",
     *     type="string",
     *     description="Adresse mail de l'utilisateur"
     * )
     * 
     * ----
     * 
     * Get the validation rules that apply to the request
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|email'
        ];
    }

    /**
     * Get the error messages for the defined validation rules
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
