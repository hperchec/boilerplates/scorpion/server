<?php

namespace App\Http\Requests\Auth;

use Illuminate\Validation\Rule;

use App\Rules\PasswordPolicy;
use App\Http\Requests\BaseRequest;
use App\Models\Error;

/**
 * @OA\Schema(schema="Requests.Auth.ResetPassword") {
 *     required={
 *         "token",
 *         "email",
 *         "password",
 *         "password_confirmation"
 *     }
 * }
 */
class ResetPasswordRequest extends BaseRequest
{

    /**
     * Determine if the user is authorized to make this request
     * @return bool
     */
    public function authorize()
    {
        // PUBLIC ROUTE
        return true;
    }

    /** 
     * @OA\Property(
     *     property="token",
     *     type="string",
     *     description="Token (temporaire) de réinitialisation de mot de passe"
     * ),
     * @OA\Property(
     *     property="email",
     *     type="string",
     *     description="Adresse mail de l'utilisateur"
     * ),
     * @OA\Property(
     *     property="password",
     *     type="string",
     *     description="Mot de passe de l'utilisateur"
     * ),
     * @OA\Property(
     *     property="password_confirmation",
     *     type="string",
     *     description="Confirmation du mot de passe de l'utilisateur"
     * )
     * 
     * ----
     * 
     * Get the validation rules that apply to the request
     * @return array
     */
    public function rules()
    {
        return [
            'token' => 'required|string',
            'email' => 'required|string|email',
            'password' => [
                'required',
                'string',
                'confirmed',
                new PasswordPolicy,
                'max:255'
            ]
        ];
    }

    /**
     * Get the error messages for the defined validation rules
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
