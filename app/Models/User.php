<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;
use Laravel\Passport\HasApiTokens;
use Illuminate\Auth\Notifications\ResetPassword;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;

use App\Notifications\VerifyEmail;
use App\Http\Traits\BaseModelTrait;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable, BaseModelTrait, HasRelationships;

    /**
     * OVERWRITTEN ELOQUENT MODEL PROPERTIES
     */

    /**
     * Table name
     * @var string
     */
    protected $table = "users";

    /**
     * The attributes that are mass assignable
     * @var array
     */
    protected $fillable = [
        'email',
        'firstname',
        'lastname',
        'password'
    ];

    /**
     * The attributes that should be hidden for arrays
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token'
    ];

    /**
     * The attributes that should be cast to native types
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime'
    ];

    /**
     * Additional attributes
     * @var array
     */
    protected $appends = [
        'last_login'
    ];

    /**
     * CUSTOM PROPERTIES
     */

    /**
     * List of relations
     */
    protected $relationNames = [
        //
    ];

     /**
     * Attribute / relation names to change when return JSON response (via toArray())
     */
    protected $attributeNamesMap = [
        //
    ];

    /**
     * RELATIONS
     */

    // ...

    /**
     * SCOPES
     */

    // ...

    /**
     * ADDITIONAL ATTRIBUTE GETTERS
     */

    /**
     * 'last_login' attribute
     */
    public function getLastLoginAttribute()
    {
        return $this->lastLogin();
    }

    /**
     * CUSTOM METHODS
     */

    /**
     * Return last user login date (based on last token)
     * @return string
     */
    public function lastLogin () {
        $lastToken = DB::table('oauth_access_tokens')->where('user_id', $this->id)->latest()->first();
        return $lastToken ? $lastToken->created_at : null;
    }

    /**
     * Generate thumbnail filename
     * @param string $imageType - Image extension
     * @return string
     */
    public function generateThumbnailFileName (string $imageType) {
        return $this->id . '_thumbnail_' . time() . '.' . $imageType;
    }

    /**
     * Return true if the user is admin
     * @return bool
     */
    public function isAdmin(){
        if($this->role == "admin"){
            return true;
        }
        return false;
    }

    /**
     * Send the password reset notification
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        // The trick is first to instantiate the notification itself
        $notification = new ResetPassword($token);
        // Then use the createUrlUsing method
        $notification->createUrlUsing(function ($user, string $token) {
            // Custom reset link -> because ui separated from server
            $appUrl = env('APP_ENV') === 'local' ? config('app.ui_dev_url') : config('app.url');
            $resetLink = url($appUrl . '/reset-password/' . $token, false);
            return $resetLink;
        });
        // Then you pass the notification
        $this->notify($notification);
    }

    /**
     * Send the email verification notification.
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        // The trick is first to instantiate the notification itself
        $notification = new VerifyEmail;
        // Then use the createUrlUsing method
        $notification->createUrlUsing(function ($user) {
            $userId = $user->getKey();
            $hash = sha1($user->getEmailForVerification());
            // Custom email verification link -> because ui separated from server
            $signedRoute = URL::temporarySignedRoute(
                'verification.verify',
                Carbon::now()->addMinutes(Config::get('auth.verification.expire', 60)),
                [
                    'id' => $userId,
                    'hash' => $hash,
                ]
            );
            // Use parse_url() function to parse the URL
            $parsedUrl = parse_url($signedRoute);
            // Get app base url
            $appUrl = env('APP_ENV') === 'local' ? config('app.ui_dev_url') : config('app.url');
            // Build full url
            $verifyEmailUrl = $appUrl . '/settings/email/verify/' . $hash . '?' . $parsedUrl['query'];
            return $verifyEmailUrl;
        });
        // Then you pass the notification
        $this->notify($notification);
    }

    /**
     * Validate password
     * @param string $password - The password to check
     * @return string|bool - If password is valid, returns crypted password, else returns false
     */
    public static function validatePassword($password)
    {
        $passwordPolicy = new PasswordPolicy();
        return $passwordPolicy->passes('password', $password);
    }

    /**
     * OVERWRITTEN BASE MODEL METHODS
     */

    /**
     * Define data visibility depending of user rights
     * @param Array $data - The original data
     * @return array
     */
    public static function dataVisibility ($data) {
        // Create base array with all data attributes to 'true'
        $visibility = array_fill_keys(array_keys($data), true);
        // Do logic here
        return $visibility;
    }

    /**
     * OVERWRITTEN ELOQUENT MODEL METHODS
     */

    /**
     * Overwrite toArray() method
     * @return array
     */
    public function toArray()
    {
        // Attributes
        $attributes = $this->attributesToArray();
        // Get related resources array
        $related = $this->relationsToArray();
        // Full data
        $data = array_merge($attributes, $related);
        // Do logic here...
        // Return formatted & cleaned object
        return static::cleanData($data);
    }

}
