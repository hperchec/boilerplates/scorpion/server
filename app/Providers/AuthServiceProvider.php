<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
use Laravel\Passport\Passport;

/**
 * @codeCoverageIgnore
 */
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Models\Model' => 'App\Policies\ModelPolicy',
        // 'App\Models\Auth' => 'App\Policies\AuthPolicy', // Auth is not a model (see below, registered in boot() method)
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        // register policies
        $this->registerPolicies();

        // Define gates for AuthPolicy
        foreach (get_class_methods('App\Policies\AuthPolicy') as $method) {
            Gate::define(Str::kebab($method), 'App\Policies\AuthPolicy@' . $method);
        }

        // Enable passport routes
        if (! $this->app->routesAreCached()) {
            Passport::routes(null, ['prefix' => 'api/oauth']);
        }
    }
}
