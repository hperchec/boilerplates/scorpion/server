{
    "openapi": "3.0.0",
    "info": {
        "title": "App API Swagger",
        "description": "App project API documentation",
        "version": "1.0.0"
    },
    "servers": [
        {
            "url": "http://127.0.0.1:8000/api",
            "description": "Local API Server"
        }
    ],
    "paths": {
        "/login": {
            "post": {
                "tags": [
                    "Auth"
                ],
                "summary": "Login user",
                "description": "**Connecter l'utilisateur à l'aide de ses identifiants**",
                "operationId": "auth.login",
                "requestBody": {
                    "description": "User credentials to authenticate",
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/Requests.Auth.Login"
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "Successful operation",
                        "content": {
                            "application/json": {}
                        }
                    },
                    "401": {
                        "description": "Unauthorized (bad credentials)"
                    },
                    "422": {
                        "description": "Unprocessable entity"
                    }
                }
            }
        },
        "/logout": {
            "post": {
                "tags": [
                    "Auth"
                ],
                "summary": "Logout authenticated user",
                "description": "**Déconnecter l'utilisateur authentifié**  \r\n               ❗ Action limitée au compte utilisateur authentifié par token\r\n           ",
                "operationId": "auth.logout",
                "requestBody": {
                    "description": "Request for logout action",
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/Requests.Auth.Logout"
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "Successful operation",
                        "content": {
                            "application/json": {}
                        }
                    },
                    "401": {
                        "description": "Unauthenticated"
                    }
                }
            }
        },
        "/me": {
            "get": {
                "tags": [
                    "Auth"
                ],
                "summary": "Retrieve authenticated user data",
                "description": "**Retrouver les données de l'utilisateur authentifié**  \r\n               ❗ Action limitée au compte utilisateur authentifié par token\r\n           ",
                "operationId": "auth.me",
                "responses": {
                    "200": {
                        "description": "Successful operation"
                    },
                    "401": {
                        "description": "Unauthenticated"
                    },
                    "403": {
                        "description": "Forbidden"
                    }
                }
            }
        },
        "/me/password": {
            "patch": {
                "tags": [
                    "Auth"
                ],
                "summary": "Update authenticated user password",
                "description": "**Modifier le mot de passe de l'utilisateur courant**  \r\n               ❗ Action limitée au compte utilisateur authentifié par token\r\n           ",
                "operationId": "password.update",
                "requestBody": {
                    "description": "User password data to update",
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/Requests.Auth.UpdatePassword"
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "Successful operation",
                        "content": {
                            "application/json": {}
                        }
                    },
                    "401": {
                        "description": "Unauthenticated"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not found"
                    },
                    "422": {
                        "description": "Unprocessable entity"
                    }
                }
            }
        },
        "/forgot-password": {
            "post": {
                "tags": [
                    "Auth"
                ],
                "summary": "Send mail to reset user password",
                "description": "**Demander l'envoi d'un mail de changement de mot de passe de l'utilisateur**",
                "operationId": "password.email",
                "requestBody": {
                    "description": "Email data for send reset password mail",
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/Requests.Auth.SendResetPasswordMail"
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "Successful operation",
                        "content": {
                            "application/json": {}
                        }
                    },
                    "422": {
                        "description": "Unprocessable entity"
                    }
                }
            }
        },
        "/reset-password": {
            "post": {
                "tags": [
                    "Auth"
                ],
                "summary": "Reset user password",
                "description": "**Réinitialiser le mot de passe de l'utilisateur**",
                "operationId": "password.reset",
                "requestBody": {
                    "description": "Password data to reset",
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/Requests.Auth.ResetPassword"
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "Successful operation",
                        "content": {
                            "application/json": {}
                        }
                    },
                    "422": {
                        "description": "Unprocessable entity"
                    }
                }
            }
        },
        "/users/create": {
            "post": {
                "tags": [
                    "Users"
                ],
                "summary": "Create user",
                "description": "**Créer un nouvel utilisateur**",
                "operationId": "users.create",
                "requestBody": {
                    "description": "User data to create",
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/Requests.User.Create"
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "Successful operation",
                        "content": {
                            "application/json": {}
                        }
                    },
                    "422": {
                        "description": "Unprocessable entity"
                    }
                }
            }
        },
        "/users/{userId}": {
            "patch": {
                "tags": [
                    "Users"
                ],
                "summary": "Update user",
                "description": "**Modifier l'utilisateur courant**  \r\n               ❗ Action limitée au compte utilisateur authentifié par token\r\n           ",
                "operationId": "users.update",
                "parameters": [
                    {
                        "name": "userId",
                        "in": "path",
                        "description": "Identifiant de l'utilisateur (ID)",
                        "required": true
                    }
                ],
                "requestBody": {
                    "description": "User data to update",
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/Requests.User.Update"
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "Successful operation"
                    },
                    "401": {
                        "description": "Unauthenticated"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not found"
                    },
                    "422": {
                        "description": "Unprocessable entity"
                    }
                }
            }
        },
        "/users/{userId}/thumbnail": {
            "get": {
                "tags": [
                    "Users"
                ],
                "summary": "Retrieve an user thumbnail",
                "description": "**Retrouver la miniature de profil d'un utilisateur**",
                "operationId": "user.thumbnail",
                "parameters": [
                    {
                        "name": "userId",
                        "in": "path",
                        "description": "Identifiant de l'utilisateur (ID)",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successful operation",
                        "content": {
                            "image/*": {}
                        }
                    },
                    "401": {
                        "description": "Unauthenticated"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not found"
                    }
                }
            },
            "post": {
                "tags": [
                    "Users"
                ],
                "summary": "Update authenticated user thumbnail",
                "description": "**Modifier la miniature (image de profil) de l'utilisateur courant**  \r\n               ❗ Action limitée au compte utilisateur authentifié par token\r\n           ",
                "operationId": "users.update.thumbnail",
                "parameters": [
                    {
                        "name": "userId",
                        "in": "path",
                        "description": "Identifiant de l'utilisateur (ID)",
                        "required": true
                    }
                ],
                "requestBody": {
                    "description": "User thumbnail data to update",
                    "required": true,
                    "content": {
                        "multipart/form-data": {
                            "schema": {
                                "$ref": "#/components/schemas/Requests.User.UpdateThumbnail"
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "Successful operation"
                    },
                    "401": {
                        "description": "Unauthenticated"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not found"
                    },
                    "422": {
                        "description": "Unprocessable entity"
                    }
                }
            }
        },
        "/users/check-email": {
            "post": {
                "tags": [
                    "Users"
                ],
                "summary": "Check email availability",
                "description": "**Tester la disponibilité d'une adresse mail**",
                "operationId": "users.checkEmail",
                "requestBody": {
                    "description": "Email data to check",
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/Requests.User.CheckEmail"
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "Successful operation",
                        "content": {
                            "application/json": {}
                        }
                    },
                    "409": {
                        "description": "Conflict"
                    },
                    "422": {
                        "description": "Unprocessable entity"
                    }
                }
            }
        },
        "/users/{userId}/email/verify/{hash}": {
            "post": {
                "tags": [
                    "Users"
                ],
                "summary": "Verify user mail address",
                "description": "**Vérification de l'adresse email de l'utilisateur**\r\n               ❗ Action limitée au compte utilisateur authentifié par token\r\n           ",
                "operationId": "verification.verify",
                "parameters": [
                    {
                        "name": "userId",
                        "in": "path",
                        "description": "Identifiant de l'utilisateur (ID)",
                        "required": true
                    },
                    {
                        "name": "hash",
                        "in": "path",
                        "description": "Hash (lien temporaire)",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successful operation"
                    },
                    "401": {
                        "description": "Unauthenticated"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not found"
                    }
                }
            }
        },
        "/users/{userId}/email/resend-verification-link": {
            "post": {
                "tags": [
                    "Users"
                ],
                "summary": "Resend mail verification link",
                "description": "**Renvoyer le lien de vérification de l'adresse email de l'utilisateur**\r\n               ❗ Action limitée au compte utilisateur authentifié par token\r\n           ",
                "operationId": "verification.send",
                "parameters": [
                    {
                        "name": "userId",
                        "in": "path",
                        "description": "Identifiant de l'utilisateur (ID)",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successful operation"
                    },
                    "401": {
                        "description": "Unauthenticated"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not found"
                    }
                }
            }
        }
    },
    "components": {
        "schemas": {
            "Requests.Auth.GetCurrentUser": {},
            "Requests.Auth.Login": {
                "properties": {
                    "email": {
                        "description": "Adresse mail de l'utilisateur",
                        "type": "string"
                    },
                    "password": {
                        "description": "Mot de passe de l'utilisateur",
                        "type": "string"
                    }
                },
                "type": "object"
            },
            "Requests.Auth.Logout": {},
            "Requests.Auth.ResetPassword": {
                "properties": {
                    "token": {
                        "description": "Token (temporaire) de réinitialisation de mot de passe",
                        "type": "string"
                    },
                    "email": {
                        "description": "Adresse mail de l'utilisateur",
                        "type": "string"
                    },
                    "password": {
                        "description": "Mot de passe de l'utilisateur",
                        "type": "string"
                    },
                    "password_confirmation": {
                        "description": "Confirmation du mot de passe de l'utilisateur",
                        "type": "string"
                    }
                },
                "type": "object"
            },
            "Requests.Auth.SendResetPasswordMail": {
                "properties": {
                    "email": {
                        "description": "Adresse mail de l'utilisateur",
                        "type": "string"
                    }
                },
                "type": "object"
            },
            "Requests.Auth.UpdatePassword": {
                "properties": {
                    "current_password": {
                        "description": "Ancien mot de passe de l'utilisateur",
                        "type": "string"
                    },
                    "password": {
                        "description": "Mot de passe de l'utilisateur",
                        "type": "string"
                    },
                    "password_confirmation": {
                        "description": "Confirmation du mot de passe de l'utilisateur",
                        "type": "string"
                    }
                },
                "type": "object"
            },
            "Requests.User.CheckEmail": {
                "properties": {
                    "email": {
                        "description": "Adresse mail de l'utilisateur",
                        "type": "string"
                    }
                },
                "type": "object"
            },
            "Requests.User.Create": {
                "properties": {
                    "firstname": {
                        "description": "Prénom de l'utilisateur",
                        "type": "string"
                    },
                    "lastname": {
                        "description": "Nom de l'utilisateur",
                        "type": "string"
                    },
                    "email": {
                        "description": "Adresse mail de l'utilisateur",
                        "type": "string"
                    },
                    "password": {
                        "description": "Mot de passe de l'utilisateur",
                        "type": "string"
                    },
                    "password_confirmation": {
                        "description": "Confirmation du mot de passe de l'utilisateur",
                        "type": "string"
                    }
                },
                "type": "object"
            },
            "Requests.User.GetUserThumbnail": {},
            "Requests.User.ResendEmailVerificationLink": {},
            "Requests.User.Update": {
                "properties": {
                    "firstname": {
                        "description": "Prénom de l'utilisateur",
                        "type": "string"
                    },
                    "lastname": {
                        "description": "Nom de l'utilisateur",
                        "type": "string"
                    },
                    "password": {
                        "description": "Mot de passe de l'utilisateur",
                        "type": "string"
                    }
                },
                "type": "object"
            },
            "Requests.User.UpdateThumbnail": {
                "properties": {
                    "password": {
                        "description": "Mot de passe de l'utilisateur",
                        "type": "string"
                    },
                    "thumbnail": {
                        "description": "Miniature (image de profil) de l'utilisateur  \r\n               ❗ *MIMES types accepted: jpeg, png, jpg, gif, svg*  \r\n               ❗ *MAX size: 2048 bytes*\r\n     *     ",
                        "type": "string",
                        "format": "binary"
                    }
                },
                "type": "object"
            }
        },
        "securitySchemes": {
            "passport": {
                "type": "oauth2",
                "description": "Laravel passport oauth2 security.",
                "in": "header",
                "scheme": "https",
                "flows": {
                    "password": {
                        "authorizationUrl": "http://localhost:8000/api/oauth/authorize",
                        "tokenUrl": "http://localhost:8000/api/oauth/token",
                        "refreshUrl": "http://localhost:8000/api/token/refresh",
                        "scopes": []
                    }
                }
            }
        }
    },
    "tags": [
        {
            "name": "Auth",
            "description": "Specific routes for authentication"
        },
        {
            "name": "Users",
            "description": "Specific routes for users"
        }
    ],
    "security": [
        []
    ]
}